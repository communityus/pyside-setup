https://gitlab.com/communityus/transfer-private

# Qt For Python

Qt For Python is the [Python Qt bindings project](http://wiki.qt.io/PySide2), providing
access to the complete Qt 5.x framework as well as to generator tools for rapidly
generating bindings for any C++ libraries.

shiboken2 is the generator used to build the bindings.

See README.pyside2.md and README.shiboken2.md for details.

# Sorce

https://code.qt.io/cgit/pyside/pyside-setup.git/

```
Clone
git://code.qt.io/pyside/pyside-setup.git
http://code.qt.io/pyside/pyside-setup.git
https://code.qt.io/pyside/pyside-setup.git
```